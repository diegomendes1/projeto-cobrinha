﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Principal : MonoBehaviour {
    public Cena cena;

    void OnTriggerEnter2D(Collider2D col) {
        if (col.gameObject.CompareTag("Comida")) {
            cena.Coletar(col.gameObject);
            cena.jogador.velocidade += 0.2f;
        } else if (col.gameObject.CompareTag("Barreira")) {
            cena.FimJogo();
        } else if (col.gameObject.CompareTag("Cobra")) {
            cena.FimJogo();
        }
    }
}
