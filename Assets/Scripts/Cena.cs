﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Cena : MonoBehaviour{
    public Camera cameraJogo;
    public Som som;

    public CenaUI cenaUI;
    public Jogador jogador;
    public Transform fila;
    public Transform posicaoColetavel;
    public GameObject[] possiveisColetaveis;

    [Header("Objetos")]
    //public GameObject pedacoPrefab;
    public GameObject pedacoPrefabPrincipal;

    [Header("Jogo")]
    public int pontosAtuais;
    public bool jogoPausado;

    [Header("Barreiras")]
    public Transform barreiraCima;
    public Transform barreiraBaixo;
    public Transform barreiraEsquerda;
    public Transform barreiraDireita;

    public Transform piso;

    public float x = 4f, y = 4f;

    void Awake() {
        som = GameObject.Find("Audio").GetComponent<Som>();
        GerarPrincipal();
        pontosAtuais = 1;
        GerarNovoColetavel();
        jogoPausado = false;
        jogador.jogoRodando = true;
        StartCoroutine(Contagem());
        cenaUI.AtualizarNumPontos(pontosAtuais, null);
    }

    IEnumerator Contagem() {
        jogador.jogoRodando = false;
        yield return new WaitForSeconds(3);
        jogador.jogoRodando = true;
    }

    public void GerarPrincipal() {
        GameObject novoModelo = Instantiate(pedacoPrefabPrincipal, jogador.transform.position, jogador.transform.rotation);
        novoModelo.AddComponent<Principal>();
        novoModelo.GetComponent<Principal>().cena = this;
        novoModelo.tag = "Player";

        jogador.AdicionarPrincipal(novoModelo);
        novoModelo.transform.SetParent(fila);
    }

    public void GerarNovoColetavel() {
        float posX = Random.Range(-x, x);
        float posY = Random.Range(-y, y);

        int colAleatorio = Random.Range(0, possiveisColetaveis.Length);

        GameObject novoColetavel = GameObject.Instantiate(possiveisColetaveis[colAleatorio], new Vector2(posX, posY), Quaternion.identity);
        posicaoColetavel = novoColetavel.transform;
        novoColetavel.GetComponent<Coletavel>().cena = this;
    }

    public void Coletar(GameObject coletavel) {
        Coletavel col = coletavel.GetComponent<Coletavel>();
        GameObject novoModelo = Instantiate(col.modelo.gameObject, jogador.ultimo.transform.position, jogador.ultimo.transform.rotation);
        jogador.AdicionarModelo(novoModelo);
        novoModelo.transform.SetParent(fila);
        Destroy(coletavel);
        GerarNovoColetavel();

        pontosAtuais++;
        cenaUI.AtualizarNumPontos(pontosAtuais, coletavel.GetComponentInChildren<SpriteRenderer>().sprite);

        AtualizarLimitesMapa();
        som.TocarSomComida();
    }

    public void AtualizarLimitesMapa() {
        x+= 0.1f;
        y+= 0.1f;
        cameraJogo.orthographicSize+= 0.1f;

        barreiraEsquerda.position = new Vector2(barreiraEsquerda.position.x - 0.1f, 0);
        barreiraEsquerda.localScale = new Vector2(1, barreiraEsquerda.localScale.y+0.1f);

        barreiraDireita.position = new Vector2(barreiraDireita.position.x + 0.1f, 0);
        barreiraDireita.localScale = new Vector2(1, barreiraDireita.localScale.y + 0.1f);

        barreiraCima.position = new Vector2(0, barreiraCima.position.y + 0.1f);
        barreiraCima.localScale = new Vector2(barreiraCima.localScale.x + 0.1f, 1);

        barreiraBaixo.position = new Vector2(0, barreiraBaixo.position.y - 0.1f);
        barreiraBaixo.localScale = new Vector2(barreiraBaixo.localScale.x + 0.1f, 1);

        piso.localScale = new Vector2(piso.localScale.x + 2f, piso.localScale.y + 2f);
    }

    public void FimJogo() {
        jogador.FimJogo();

        bool isMelhor;
        if (PlayerPrefs.GetInt("Melhor") < pontosAtuais) {
            PlayerPrefs.SetInt("Melhor", pontosAtuais);
            isMelhor = true;
        } else {
            isMelhor = false;
        }
        
        cenaUI.FimJogo(pontosAtuais, isMelhor);
        som.TocarSomFimJogo();
    }

    public void ReiniciarJogo() {
        SceneManager.LoadScene(1);
    }

    public void SairJogo() {
        GameObject.Destroy(som.gameObject);
        SceneManager.LoadScene(0);
    }
}
