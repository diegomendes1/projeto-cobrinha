﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControleJogo : MonoBehaviour{
    public Cena cena;
    public Joystick joystick;


    void FixedUpdate() {
            Vector2 direcao = new Vector2(joystick.Horizontal, joystick.Vertical);
            if (direcao != Vector2.zero) {
                cena.jogador.GirarJogador(direcao);
            }
    }

}
