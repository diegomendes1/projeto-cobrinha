﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour {
	public Animator transicao;
    public Text melhorTexto;
    public Som som;

    void Start() {
        GameObject.DontDestroyOnLoad(som.gameObject);

        if (PlayerPrefs.HasKey("Melhor")) {
            melhorTexto.text = "Melhor: " + PlayerPrefs.GetInt("Melhor").ToString();
        } else {
            PlayerPrefs.SetInt("Melhor", 0);
            melhorTexto.text = "Melhor: 0";
        }
    }


    //Inicia a transicao
    public void IniciarJogo(){
		transicao.SetTrigger("IniciandoPartida");
	}

	public void CarregarCenaJogo(){
		SceneManager.LoadScene(1);
	}

	public void SairJogo(){
		Application.Quit();
	}
}
