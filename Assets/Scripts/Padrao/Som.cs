﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Som : MonoBehaviour {
	bool isSom = false;
    public AudioSource musica;
    public AudioSource somComida;
    public AudioSource somFimJogo;
    public Toggle somToggle;

    void Start(){
        if (PlayerPrefs.HasKey("som")) {
            if (PlayerPrefs.GetInt("som") == 1) {
                isSom = true;
                somToggle.isOn = true;
                AtivarSom();
            } else {
                DesativarSom();
                somToggle.isOn = false;
                isSom = false;
            }
        } else {
            PlayerPrefs.SetInt("som", 1);
            AtivarSom();
            isSom = true;
        }
    }

    public void AtivarSom() {
        musica.Play();
    }

    public void DesativarSom() {
        musica.Stop();
    }

    public void AtivarDesativarSom(){
        isSom = somToggle.isOn;

        if (isSom) {
            AtivarSom();
            PlayerPrefs.SetInt("som", 1);
        } else {
            DesativarSom();
            PlayerPrefs.SetInt("som", 0);
        }
	}

    public void TocarSomComida() {
        if (isSom) {
            somComida.Play();
        }
    }

    public void TocarSomFimJogo() {
        if (isSom) {
            somFimJogo.Play();
        }
    }
}
