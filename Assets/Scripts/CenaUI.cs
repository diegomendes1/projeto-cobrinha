﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CenaUI : MonoBehaviour{
    public Cena cena;

    public Text pontos;

    public GameObject painelFimJogo;
    public Text pontosText;
    public GameObject melhorText;

    public Image ultimaFrutaComida;

    public void AtualizarNumPontos(int numPontos, Sprite fruta) {
        if (fruta != null) {
            ultimaFrutaComida.sprite = fruta;
        }
       
        pontos.text = numPontos.ToString();
    }

    public void FimJogo(int pontos, bool isMelhor) {
        pontosText.text = pontos.ToString();

        if (isMelhor) {
            melhorText.SetActive(true);
        } else {
            melhorText.SetActive(false);
        }

        painelFimJogo.SetActive(true);
    }

    public void BotaoReiniciar() {
        cena.ReiniciarJogo();
    }

    public void BotaoSair() {
        cena.SairJogo();
    }
}
