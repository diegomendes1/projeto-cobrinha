﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jogador : MonoBehaviour{
    public Rigidbody2D corpoRigido2D;
    public Transform alvo;
    public float velocidade = 10f;
    public List<GameObject> fila;
    public GameObject primeiro;
    public GameObject ultimo;
    public bool jogoRodando;

    void FixedUpdate() {
        if (!jogoRodando) {
            return;
        }
        AtualizarPosicoes();
        corpoRigido2D.velocity = alvo.up * velocidade;
    }

    public void GirarJogador(Vector2 direcao) {
        float angulo = Mathf.Atan2(direcao.x, direcao.y) * Mathf.Rad2Deg;
        Quaternion rotacao = Quaternion.AngleAxis(angulo, Vector3.back);
        transform.rotation = Quaternion.Slerp(alvo.rotation, rotacao, velocidade * Time.deltaTime);
    }

    public void AtualizarPosicoes() {
        float step = velocidade/30;
        for (int i = fila.Count - 1; i > 0; i--) {
            fila[i].transform.position = Vector2.Lerp(fila[i].transform.position, fila[i - 1].transform.position, step);
            fila[i].transform.rotation = Quaternion.Lerp(fila[i].transform.rotation, fila[i - 1].transform.rotation, step);
        }
        fila[0].transform.position = Vector2.Lerp(fila[0].transform.position, alvo.position, step);
        fila[0].transform.rotation = Quaternion.Lerp(fila[0].transform.rotation, alvo.rotation, step);
    }

    public void AdicionarModelo(GameObject novoModelo) {
        fila.Add(novoModelo);
        ultimo = novoModelo;
        novoModelo.name = "Modelo " + fila.Count;
        if (fila.Count < 4) {
            novoModelo.GetComponent<Collider2D>().enabled = false;
        }
    }

    public void AdicionarPrincipal(GameObject novoPrincipal) {
        fila.Add(novoPrincipal);
        primeiro = novoPrincipal;
        ultimo = novoPrincipal;
        novoPrincipal.name = "Principal";
    }

    public void FimJogo() {
        corpoRigido2D.velocity = 0f * transform.forward;
        jogoRodando = false;
        for (int i = 0; i < fila.Count; i++) {
            fila[i].GetComponent<Modelo>().Destroir();
        }
    }
}
